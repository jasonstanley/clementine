<?php
namespace jasonstanley\clementine\Generators;

use jasonstanley\clementine\Schema\ColumnAbstract;

interface GeneratorInterface
{

	/**
	 * Determine if the rule applies.
	 *
	 * @param ColumnAbstract $column
	 * @return bool
	 */
	public function applies(ColumnAbstract $column): bool;

	/**
	 * Return a value for the column
	 *
	 * @param ColumnAbstract $column
	 * @return mixed
	 */
	public function getValue(ColumnAbstract $column);

}