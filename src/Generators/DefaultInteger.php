<?php
namespace jasonstanley\clementine\Generators;


use Faker\Factory;
use jasonstanley\clementine\Schema\ColumnAbstract;
use jasonstanley\clementine\Schema\Integer;

class DefaultInteger implements GeneratorInterface
{

	/**
	 * @param ColumnAbstract $column
	 * @return bool
	 */
	public function applies(ColumnAbstract $column): bool
	{
		return $column instanceof Integer && ! $column->autoIncrements();
	}

	/**
	 * @param ColumnAbstract $column
	 * @todo I don't like this. Randomness vs not generating out of range values.
	 * @return int
	 */
	public function getValue(ColumnAbstract $column)
	{
		$value = mt_rand(1, mt_getrandmax());
		return (int)substr($value, 0, $column->getFieldLength());
	}

}