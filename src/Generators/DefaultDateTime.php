<?php


namespace jasonstanley\clementine\Generators;


use Faker\Factory;
use jasonstanley\clementine\Schema\ColumnAbstract;
use jasonstanley\clementine\Schema\DateTime;

class DefaultDateTime implements GeneratorInterface
{

	public function applies(ColumnAbstract $column): bool
	{
		return $column instanceof DateTime;
	}

	public function getValue(ColumnAbstract $column)
	{
		$faker = Factory::create();
		return $faker->dateTime()->format('Y-m-d H:i:s');
	}

}