<?php


namespace jasonstanley\clementine\Generators;


interface Settable
{

	/**
	 * @param string $comparisonColumn
	 */
	public function setComparisonColumn(string $comparisonColumn);

}