<?php


namespace jasonstanley\clementine\Generators;


use jasonstanley\clementine\Schema\ColumnAbstract;
use jasonstanley\clementine\Schema\Enum;

class DefaultEnum implements GeneratorInterface
{

	public function applies(ColumnAbstract $column): bool
	{
		return $column instanceof Enum;
	}

	public function getValue(ColumnAbstract $column)
	{
		$acceptedValues = $column->getAcceptedValues();
		return $acceptedValues[rand(0, count($acceptedValues) - 1)];
	}


}