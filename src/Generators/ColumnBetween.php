<?php
namespace jasonstanley\clementine\Generators;
use Faker\Factory;
use jasonstanley\clementine\Schema\ColumnAbstract;
use jasonstanley\clementine\Schema\Integer;

class ColumnBetween implements GeneratorInterface, Settable
{

	/**
	 * @var mixed
	 */
	private $value1;

	/**
	 * @var mixed
	 */
	private $value2;

	/**
	 * @var string
	 */
	private $comparisonColumn;

	/**
	 * ColumnBetween constructor.
	 * @param string $comparisonColumn
	 * @param $value1
	 * @param $value2
	 */
	public function __construct(string $comparisonColumn, $value1, $value2) {
		$this->comparisonColumn = $comparisonColumn;
		$this->value1 = $value1;
		$this->value2 = $value2;
	}

	/**
	 * @param string $comparisonColumn
	 */
	public function setComparisonColumn(string $comparisonColumn)
	{
		$this->comparisonColumn = $comparisonColumn;
	}

	/**
	 * @param ColumnAbstract $column
	 * @return bool
	 */
	public function applies(ColumnAbstract $column): bool
	{
		return $column->getFieldName() === $this->comparisonColumn;
	}

	/**
	 * @param ColumnAbstract $column
	 * @return mixed
	 */
	public function getValue(ColumnAbstract $column)
	{
		if (is_int($this->value1) && is_int($this->value2)) {
			return $this->getRandomInt();
		}

		if ($this->value1 instanceof \DateTime && $this->value2 instanceof \DateTime) {
			$dateTime = $this->getRandomDate();
			if ($column instanceof Integer) {
				return $dateTime->getTimestamp();
			}

			return $this->getRandomDate()->format('Y-m-d H:i:s');
		}

		return $this->value;
	}

	/**
	 * @return int|mixed
	 */
	private function getRandomInt() {
		$diff = $this->value2 - $this->value1;
		return $this->value1 + mt_rand(0, $diff);
	}

	/**
	 * @return \DateTime
	 */
	private function getRandomDate() {
		$faker = Factory::create();
		return $faker->dateTimeBetween($this->value1, $this->value2);
	}

}