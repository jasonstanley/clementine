<?php


namespace jasonstanley\clementine\Generators;


use jasonstanley\clementine\Schema\ColumnAbstract;

class ColumnEqualTo implements GeneratorInterface, Settable
{

	/**
	 * @var mixed
	 */
	private $value;

	/**
	 * @var string
	 */
	private $comparisonColumn;

	/**
	 * ColumnEqualTo constructor.
	 * @param string $comparisonColumn
	 * @param $equalToValue
	 */
	public function __construct(string $comparisonColumn, $equalToValue) {
		$this->comparisonColumn = $comparisonColumn;
		$this->value = $equalToValue;
	}

	/**
	 * @param string $comparisonColumn
	 */
	public function setComparisonColumn(string $comparisonColumn)
	{
		$this->comparisonColumn = $comparisonColumn;
	}

	public function applies(ColumnAbstract $column): bool
	{
		return $column->getFieldName() === $this->comparisonColumn;
	}

	public function getValue(ColumnAbstract $column)
	{
		return $this->value;
	}

}