<?php


namespace jasonstanley\clementine\Generators;


use Faker\Factory;
use jasonstanley\clementine\Schema\ColumnAbstract;
use jasonstanley\clementine\Schema\Varchar;

class DefaultVarchar implements GeneratorInterface
{

	/**
	 * @param ColumnAbstract $column
	 * @return bool
	 */
	public function applies(ColumnAbstract $column): bool
	{
		return $column instanceof Varchar;
	}

	/**
	 * @param ColumnAbstract $column
	 * @return string
	 */
	public function getValue(ColumnAbstract $column)
	{
		// Faker requires at least 5 characters to generate text.
		if ($column->getFieldLength() < 5) {
			return substr(Factory::create()->text(5), 0, $column->getFieldLength());
		}

		 return Factory::create()->text($column->getFieldLength());
	}

}