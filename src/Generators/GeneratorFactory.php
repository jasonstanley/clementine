<?php


namespace jasonstanley\clementine\Generators;


use jasonstanley\clementine\Config;
use jasonstanley\clementine\Schema\ColumnAbstract;

class GeneratorFactory
{

	/**
	 * @var array
	 */
	private $generators = [];

	/**
	 * @var array
	 */
	private $rules = [];

	/**
	 * GeneratorFactory constructor.
	 * @param Config $config
	 */
	public function __construct(Config $config) {
		$this->rules = $config->getRules();

		$this->generators = [
			new DefaultInteger(),
			new DefaultVarchar(),
			new DefaultText(),
			new DefaultDateTime(),
			new DefaultEnum(),
		];
	}

	/**
	 * @param ColumnAbstract $column
	 * @return GeneratorInterface?
	 */
	public function create(ColumnAbstract $column) {
		// A Rule is a Generator class a client has chosen to run on a specific column.
		foreach($this->rules as $rule) {
			if ($rule->applies($column)) {
				return $rule;
			}
		}

		foreach($this->generators as $generator) {
			if ($generator->applies($column)) {
				return $generator;
			}
		}
	}

}