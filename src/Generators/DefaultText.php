<?php


namespace jasonstanley\clementine\Generators;


use Faker\Factory;
use jasonstanley\clementine\Schema\ColumnAbstract;
use jasonstanley\clementine\Schema\Text;

class DefaultText implements GeneratorInterface
{

	public function applies(ColumnAbstract $column): bool
	{
		return $column instanceof Text;
	}

	public function getValue(ColumnAbstract $column)
	{
		$faker = Factory::create();
		return substr($faker->paragraphs(5, true), 0, $column->getFieldLength());
	}

}