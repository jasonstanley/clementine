<?php


namespace jasonstanley\clementine;


use jasonstanley\clementine\Generators\GeneratorInterface;

class Config
{

	/**
	 * @var string
	 */
	private $tableName;

	/**
	 * @var int
	 */
	private $rows;

	/**
	 * @var Rules
	 */
	private $rules = [];

	/**
	 * @return string
	 */
	public function getTableName(): string
	{
		return $this->tableName;
	}

	/**
	 * @param string $tableName
	 * @return Config
	 */
	public function setTableName(string $tableName): Config
	{
		$this->tableName = $tableName;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getRows(): int
	{
		return $this->rows;
	}

	/**
	 * @param int $rows
	 * @return Config
	 */
	public function setRows(int $rows): Config
	{
		$this->rows = $rows;
		return $this;
	}

	/**
	 * @param string $columnName
	 * @param GeneratorInterface $generator
	 */
	public function setRule(string $columnName, GeneratorInterface $generator) {
		$this->rules[$columnName] = $generator;
	}

	/**
	 * @return array
	 */
	public function getRules(): array {
		return $this->rules;
	}

	/**
	 * Determine if the config object is valid.
	 *
	 * @return bool
	 */
	public function isValid() {
		return $this->tableName && $this->rows > 0;
	}

}