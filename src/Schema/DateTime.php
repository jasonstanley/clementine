<?php


namespace jasonstanley\clementine\Schema;


class DateTime extends ColumnAbstract
{

	/**
	 * Return the length of the column
	 *
	 * @param array $describedColumn
	 * @return mixed
	 */
	public function parseFieldLength(array $describedColumn) {
		return 0;
	}

}