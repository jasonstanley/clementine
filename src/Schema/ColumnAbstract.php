<?php
namespace jasonstanley\clementine\Schema;

abstract class ColumnAbstract
{

	/**
	 * @var string
	 */
	protected $fieldName;

	/**
	 * @var mixed
	 */
	protected $fieldLength;

	/**
	 * @var bool
	 */
	protected $nullable;

	/**
	 * @var bool
	 */
	protected $primary;

	/**
	 * @var bool
	 */
	protected $autoIncrements;

	/**
	 * ColumnAbstract constructor.
	 * @param array $describedColumn
	 */
	public function __construct(array $describedColumn) {
		$this->fieldName = $describedColumn['Field'];
		$this->nullable = $describedColumn['Null'] === 'YES';
		$this->autoIncrements = $describedColumn['Extra'] === 'auto_increment';
		$this->fieldLength = $this->parseFieldLength($describedColumn);
	}

	/**
	 * Return the length of the column
	 *
	 * @param array $describedColumn
	 * @return mixed
	 */
	public abstract function parseFieldLength(array $describedColumn);

	/**
	 * @return string
	 */
	public function getFieldName(): string
	{
		return $this->fieldName;
	}

	/**
	 * @param string $fieldName
	 * @return ColumnAbstract
	 */
	public function setFieldName(string $fieldName): ColumnAbstract
	{
		$this->fieldName = $fieldName;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isNullable(): bool
	{
		return $this->nullable;
	}

	/**
	 * @param bool $nullable
	 * @return ColumnAbstract
	 */
	public function setNullable(bool $nullable): ColumnAbstract
	{
		$this->nullable = $nullable;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isPrimary(): bool
	{
		return $this->primary;
	}

	/**
	 * @param bool $primary
	 * @return ColumnAbstract
	 */
	public function setPrimary(bool $primary): ColumnAbstract
	{
		$this->primary = $primary;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function autoIncrements(): bool
	{
		return $this->autoIncrements;
	}

	/**
	 * @param bool $autoIncrements
	 * @return ColumnAbstract
	 */
	public function setAutoIncrements(bool $autoIncrements): ColumnAbstract
	{
		$this->autoIncrements = $autoIncrements;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFieldLength()
	{
		return $this->fieldLength;
	}

	/**
	 * @param $fieldLength
	 * @return ColumnAbstract
	 */
	public function setFieldLength($fieldLength)
	{
		$this->fieldLength = $fieldLength;
		return $this;
	}

}