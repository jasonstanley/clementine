<?php
namespace jasonstanley\clementine\Schema;


class Table
{

	/**
	 * @var []Column
	 */
	private $columns = [];

	/**
	 * @var string
	 */
	private $tableName = '';

	/**
	 * Table constructor.
	 * @param string $tableName
	 */
	public function __construct(string $tableName)
	{
		$this->tableName = $tableName;
	}

	/**
	 * @param ColumnAbstract $column
	 */
	public function addColumn(ColumnAbstract $column) {
		$this->columns[] = $column;
	}

	/**
	 * @return ColumnAbstract[]
	 */
	public function getColumns(): array {
		return $this->columns;
	}

	/**
	 * @return string
	 */
	public function getTableName(): string
	{
		return $this->tableName;
	}

}