<?php


namespace jasonstanley\clementine\Schema;


class Text extends ColumnAbstract
{
	/**
	 * Return the length of the column
	 *
	 * @param array $describedColumn
	 * @return mixed
	 */
	public function parseFieldLength(array $describedColumn) {
		switch($describedColumn['Type']) {
			case 'tinytext':
				return 255;
			case 'text':
				return 65535;
			case 'mediumtext':
				return 16777215;
			case 'longtext':
				return 4294967295;
		}
	}

}