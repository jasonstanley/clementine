<?php


namespace jasonstanley\clementine\Schema;


class Enum extends ColumnAbstract
{

	/**
	 * @var array
	 */
	protected $acceptedValues = [];

	/**
	 * ColumnAbstract constructor.
	 * @param array $describedColumn
	 */
	public function __construct(array $describedColumn) {
		parent::__construct($describedColumn);
		$this->parseValues($describedColumn);
	}

	/**
	 * Return the length of the column
	 *
	 * @param array $describedColumn
	 * @return mixed
	 */
	public function parseFieldLength(array $describedColumn) {
		return 0;
	}

	/**
	 * @return array
	 */
	public function getAcceptedValues(): array
	{
		return $this->acceptedValues;
	}

	/**
	 * @param array $describedColumn
	 */
	private function parseValues(array $describedColumn) {
		$valuesString = substr($describedColumn['Type'], 5, -1);
		foreach(explode(',', $valuesString) as $value) {
			$this->acceptedValues[] = str_replace("'", '', $value);
		}
	}

}