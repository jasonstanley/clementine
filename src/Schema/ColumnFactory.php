<?php


namespace jasonstanley\clementine\Schema;


use phpDocumentor\Reflection\Types\Float_;

class ColumnFactory
{

	public static function make(array $describedColumn) {
		self::validateDescribedColumn($describedColumn);

		$type = explode('(', $describedColumn['Type'])[0];
		switch ($type) {
			case 'int':
			case 'tinyint':
			case 'smallint':
			case 'mediumint':
			case 'bigint':
				return new Integer($describedColumn);
			case 'varchar':
				return new Varchar($describedColumn);
			case 'tinytext':
			case 'text':
			case 'mediumtext':
			case 'longtext':
				return new Text($describedColumn);
			case 'enum':
				return new Enum($describedColumn);
			case 'datetime':
			case 'timestamp':
				return new DateTime($describedColumn);
		}


	}

	/**
	 * Is this a waste of effort adding unnecessary overhead? Yes. Yes it probably is.
	 * @todo Make $describedColumn an object maybe? Maybe not? But perhaps yes?
	 *
	 * @param array $describedColumn
	 */
	private static function validateDescribedColumn(array $describedColumn) {
		if (count($describedColumn) !== 6) {
			throw new \InvalidArgumentException("Expected {$describedColumn} to contain 6 values");
		}

		if (! isset($describedColumn['Field']) ||
			! isset($describedColumn['Type']) ||
			! isset($describedColumn['Null']) ||
			! isset($describedColumn['Key']) ||
			! array_key_exists('Default', $describedColumn) ||
			! isset($describedColumn['Extra'])
		) {
			throw new \InvalidArgumentException("I want the output of DESCRIBE. You have specified something different.");
		}
	}

}