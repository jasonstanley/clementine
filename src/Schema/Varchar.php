<?php


namespace jasonstanley\clementine\Schema;


class Varchar extends ColumnAbstract
{
	/**
	 * Return the length of the column
	 *
	 * @param array $describedColumn
	 * @return mixed
	 */
	public function parseFieldLength(array $describedColumn) {
		$output = [];
		preg_match("/\d+/", $describedColumn['Type'], $output);
		return $output[0];
	}

}