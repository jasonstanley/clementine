<?php
namespace jasonstanley\clementine\Stores;
use jasonstanley\clementine\Schema\ColumnAbstract;
use jasonstanley\clementine\Schema\ColumnFactory;
use jasonstanley\clementine\Schema\Table;
use \PDO;
use Prophecy\Exception\InvalidArgumentException;

class PDOStore implements StoreInterface
{

	/**
	 * @var string
	 */
	private $tableName;

	/**
	 * @var PDO
	 */
	private $pdo;

	/**
	 * Schema constructor.
	 * @param PDO $pdo
	 */
	public function __construct(PDO $pdo) {
		$this->pdo = $pdo;
	}

	/**
	 * Create a table object for the specified table.
	 *
	 * @param string $tableName
	 * @return mixed
	 */
	public function readTable(string $tableName): Table {
		$stmt = $this->pdo->query("DESCRIBE {$tableName}");
		if ( ! $stmt) {
			throw new InvalidArgumentException("Table {$tableName} does not exist.");
		}
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$table = new Table($tableName);
		foreach ($results as $result) {
			$table->addColumn(ColumnFactory::make($result));
		}

		return $table;
	}

	/**
	 * Insert a row.
	 *
	 * @param Table $table
	 * @param array $rowData
	 * @return mixed
	 */
	public function createRow(Table $table, array $rowData) {
		if (count($rowData) === 0) {
			return;
		}

		$params = [];
		$sql = "INSERT INTO `{$table->getTableName()}` (";
		foreach($rowData as $column => $value) {
			$sql .= "`{$column}`,";
			$params[] = $value;
		}

		$sql = substr($sql, 0, -1);
		$sql .= ") VALUES (";
		$sql .= str_repeat("?,", count($params));
		$sql = substr($sql, 0, -1);
		$sql .= ");";

		$stmt = $this->pdo->prepare($sql);
		$stmt->execute($params);
	}

}