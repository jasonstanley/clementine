<?php
namespace jasonstanley\clementine\Stores;
use jasonstanley\clementine\Schema\Table;

interface StoreInterface
{

	/**
	 * Create a table object for the specified table.
	 *
	 * @param string $tableName
	 * @return mixed
	 */
	public function readTable(string $tableName): Table;

	/**
	 * Insert a row.
	 *
	 * @param Table $table
	 * @param array $rowRowData
	 * @return mixed
	 */
	public function createRow(Table $table, array $rowRowData);

}