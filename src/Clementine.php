<?php
namespace jasonstanley\clementine;
use jasonstanley\clementine\Generators\ColumnBetween;
use jasonstanley\clementine\Generators\ColumnEqualTo;
use jasonstanley\clementine\Generators\GeneratorFactory;
use jasonstanley\clementine\Generators\GeneratorInterface;
use jasonstanley\clementine\Generators\Settable;
use jasonstanley\clementine\Stores\StoreInterface;
use \PDO;

class Clementine
{

	/**
	 * @var StoreInterface
	 */
	private $store;

	/**
	 * @var Config
	 */
	private $config;

	/**
	 * Clementine constructor.
	 * @param StoreInterface $store
	 */
	public function __construct(StoreInterface $store) {
		$this->store = $store;
		$this->reset();
	}

	/**
	 * Set the table name to add data into.
	 *
	 * @param string $tableName
	 * @return $this
	 */
	public function table(string $tableName) {
		$this->config->setTableName($tableName);
		return $this;
	}

	/**
	 * Specify a column that should have a specific generator.
	 *
	 * @param string $columnName
	 * @param GeneratorInterface $generator
	 * @return Clementine
	 */
	public function with(string $columnName, GeneratorInterface $generator) {
		// This is for convenience to get things working. The application of generators needs a rethink.
		if ($generator instanceof Settable) {
			$generator->setComparisonColumn($columnName);
		}

		$this->config->setRule($columnName, $generator);
		return $this;
	}

	/**
	 * Make some data.
	 *
	 * @param int $number
	 */
	public function make(int $number) {
		$this->config->setRows($number);
		if (!$this->config->isValid()) {
			throw new \InvalidArgumentException('A table name and amount of rows must be specified to generate data.');
		}

		// Analyse table schema.
		$table = $this->store->readTable($this->config->getTableName());

		// Loop throw table to generate data.
		$ruleFactory = new GeneratorFactory($this->config);
		for ($i=0; $i < $this->config->getRows(); $i++) {
			$inserts = [];
			foreach ($table->getColumns() as $column) {
				if ($rule = $ruleFactory->create($column)) {
					$inserts[$column->getFieldName()] = $rule->getValue($column);
				}
			}

			$this->store->createRow($table, $inserts);
		}

		$this->reset();
	}

	/**
	 * @param $value
	 * @return ColumnEqualTo
	 */
	public function equalTo($value) {
		return new ColumnEqualTo('', $value);
	}

	/**
	 * @param $value1
	 * @param $value2
	 * @return ColumnBetween
	 */
	public function between($value1, $value2) {
		return new ColumnBetween('', $value1, $value2);
	}

	/**
	 * Set the config
	 */
	private function reset() {
		$this->config = new Config();
	}

}