#### Clementine
Clementine generates mock data to any database table. It does this by reading the database schema and inserting an appropriate value.

Clementine also supports defined values. For example, you may want to create 20 comments which are owned by a specified user id.

#### Work In Progress
Clementine is a work in progress. It is lacking support for a number of column types. Its test coverage is patchy. This is a weekend project mostly fueled by rum. Some columns like Text always return the same value. This said what is supported should work well. 

#### Usage

	$clem = new \jasonstanley\clementine\Clementine(new \jasonstanley\clementine\Stores\PDOStore($somePDOConnection));
	
	// Make 5 users.
	$clem->table('users')->make(5);
	
	// Make 5 posts with a specific user id.
	$clem->table('posts')->with('user_id', $clem->equalTo(6))->make(5);
	
	// Make 10 tokens which expired in the past 24 hours.
	$clem->table('tokens')->with('expires', $clem->between(new DateTime('-1 day'), new DateTime())->make(10);

	// Note, you can pass your own GeneratorInterface as the second paramter of the with() method if you have any specilised rules.  

#### Working Towards Version 0.3
* [ ] Tables should be analysed and validated
* [ ] Add support for reusable rulesets
* [ ] Add a default ruleset
* [ ] Add support for float / decimal.
* [ ] Add common specialised rules. email / password / IP / first_name/forename, name