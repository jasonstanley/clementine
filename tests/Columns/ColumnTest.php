<?php
namespace Test\Columns;

use jasonstanley\clementine\Schema\Integer;
use PHPUnit\Framework\TestCase;

class ColumnTest extends TestCase
{

	public function testFieldLengthIsDeterminedWithUnsignedColumns() {
		$int = new Integer([
			"Field" => "user_id",
			"Type" => "int(10) unsigned",
			"Null" => "NO",
			"Key" => "PRI",
			"Default" => NULL,
			"Extra" => "auto_increment"
		]);

		$this->assertEquals(10, $int->getFieldLength());
	}

}