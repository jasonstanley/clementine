<?php

namespace Test\Generators;

use jasonstanley\clementine\Generators\DefaultDateTime;
use jasonstanley\clementine\Schema\DateTime;
use jasonstanley\clementine\Schema\Integer;
use \PHPUnit\Framework\TestCase;

class DefaultDateTimeTest extends TestCase
{

	public function testDateTimeGeneratesAValidDate()
	{
		$column = $this->makeDateTimeColumn();

		$generator = new DefaultDateTime();
		$value = $generator->getValue($column);
		$this->assertInstanceOf('DateTime', \DateTime::createFromFormat('Y-m-d H:i:s', $value));
	}

	public function testDateTimeAppliesOnlyToDateTimeColumns()
	{
		$dateTimeColumn = $this->makeDateTimeColumn();
		$intColumn = $this->makeIntColumn();
		$generator = new DefaultDateTime();
		$this->assertTrue($generator->applies($dateTimeColumn));
		$this->assertFalse($generator->applies($intColumn));
	}

	public function testDateTimeGeneratesARandomDate()
	{
		$column = $this->makeDateTimeColumn();
		$generator = new DefaultDateTime();
		$this->assertNotEquals($generator->getValue($column), $generator->getValue($column));
	}

	private function makeDateTimeColumn()
	{
		return (new DateTime([
			"Field" => "datetime",
			"Type" => "datetime",
			"Null" => "NO",
			"Key" => "",
			"Default" => NULL,
			"Extra" => ""
		]));
	}

	private function makeIntColumn()
	{
		return (new Integer([
			"Field" => "int",
			"Type" => "int(11)",
			"Null" => "NO",
			"Key" => "",
			"Default" => NULL,
			"Extra" => ""
		]));
	}

}