<?php

namespace Test\Generators;

use jasonstanley\clementine\Generators\DefaultVarchar;
use jasonstanley\clementine\Schema\Integer;
use jasonstanley\clementine\Schema\Varchar;
use PHPUnit\Framework\TestCase;

class DefaultVarcharTest extends TestCase
{

	public function testVarcharGeneratesAValidString()
	{
		$column = $this->makeIntColumn();

		$generator = new DefaultVarchar();
		$this->assertInternalType('string', $generator->getValue($column));
	}

	public function testVarcharAppliesOnlyToVarcharColumns()
	{
		$intColumn = $this->makeIntColumn();
		$varcharColumn = $this->makeVarcharColumn();
		$generator = new DefaultVarchar();
		$this->assertTrue($generator->applies($varcharColumn));
		$this->assertFalse($generator->applies($intColumn));
	}

	public function testVarcharGeneratesARandomValue()
	{
		$column = $this->makeVarcharColumn();
		$generator = new DefaultVarchar();
		$this->assertNotEquals($generator->getValue($column), $generator->getValue($column));
	}

	public function testVarcharGeneratesAValueThatFitsTheColumnLength()
	{
		$generator = new DefaultVarchar();
		for ($i = 1; $i < 256; $i++) {
			$value = $generator->getValue($this->makeVarcharColumn($i));
			$this->assertLessThanOrEqual($i, mb_strlen($value));
		}
	}

	private function makeVarcharColumn($length = 255)
	{
		return (new Varchar([
			"Field" => "varchar",
			"Type" => "varchar({$length})",
			"Null" => "NO",
			"Key" => "",
			"Default" => NULL,
			"Extra" => ""
		]));
	}

	private function makeIntColumn()
	{
		return (new Integer([
			"Field" => "int",
			"Type" => "int(11)",
			"Null" => "NO",
			"Key" => "",
			"Default" => NULL,
			"Extra" => ""
		]));
	}

}