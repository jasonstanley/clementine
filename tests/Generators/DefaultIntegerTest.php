<?php

namespace Test\Generators;

use \jasonstanley\clementine\Schema\DateTime;
use jasonstanley\clementine\Generators\DefaultInteger;
use jasonstanley\clementine\Schema\Integer;
use \PHPUnit\Framework\TestCase;

class DefaultIntegerTest extends TestCase
{

	public function testDateTimeGeneratesAValidDate()
	{
		$column = $this->makeIntColumn();

		$generator = new DefaultInteger();
		$this->assertInternalType('int', $generator->getValue($column));
	}

	public function testIntegerAppliesOnlyToIntegerColumns()
	{
		$intColumn = $this->makeIntColumn();
		$dateTimeColumn = $this->makeDateTimeColumn();
		$generator = new DefaultInteger();
		$this->assertTrue($generator->applies($intColumn));
		$this->assertFalse($generator->applies($dateTimeColumn));
	}

	public function testIntegerGeneratesARandomValue()
	{
		$column = $this->makeIntColumn();
		$generator = new DefaultInteger();
		$this->assertNotEquals($generator->getValue($column), $generator->getValue($column));
	}

	public function testIntGeneratesAValueThatFitsTheColumnLength()
	{
		$generator = new DefaultInteger();
		for ($i = 1; $i < 11; $i++) {
			$value = $generator->getValue($this->makeIntColumn($i));
			$this->assertLessThanOrEqual($i, mb_strlen($value));
		}
	}

	public function testIntValuesAreNotOutOfRange() {
		$generator = new DefaultInteger();
		for ($i = 1; $i < 100; $i++) {
			$value = $generator->getValue($this->makeIntColumn(11));
			$this->assertLessThanOrEqual(2147483647, $value);
		}
	}

	private function makeDateTimeColumn()
	{
		return (new DateTime([
			"Field" => "datetime",
			"Type" => "datetime",
			"Null" => "NO",
			"Key" => "",
			"Default" => NULL,
			"Extra" => ""
		]));
	}

	private function makeIntColumn($fieldLength=11)
	{
		return (new Integer([
			"Field" => "int",
			"Type" => "int({$fieldLength})",
			"Null" => "NO",
			"Key" => "",
			"Default" => NULL,
			"Extra" => ""
		]));
	}

}