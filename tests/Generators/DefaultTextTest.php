<?php

namespace Test\Generators;

use jasonstanley\clementine\Generators\DefaultText;
use jasonstanley\clementine\Schema\Integer;
use jasonstanley\clementine\Schema\Text;
use PHPUnit\Framework\TestCase;

class DefaultTextTest extends TestCase
{

	public function testTextGeneratesAValidString()
	{
		$column = $this->makeIntColumn();

		$generator = new DefaultText();
		$this->assertInternalType('string', $generator->getValue($column));
	}

	public function testTextAppliesOnlyToTextColumns()
	{
		$intColumn = $this->makeIntColumn();
		$textColumn = $this->makeTextColumn();
		$generator = new DefaultText();
		$this->assertTrue($generator->applies($textColumn));
		$this->assertFalse($generator->applies($intColumn));
	}

	public function testTextGeneratesARandomValue()
	{
		$column = $this->makeTextColumn();
		$generator = new DefaultText();
		$this->assertNotEquals($generator->getValue($column), $generator->getValue($column));
	}

	private function makeTextColumn()
	{
		return (new Text([
			"Field" => "text",
			"Type" => "text",
			"Null" => "NO",
			"Key" => "",
			"Default" => NULL,
			"Extra" => ""
		]));
	}

	private function makeIntColumn()
	{
		return (new Integer([
			"Field" => "int",
			"Type" => "int(11)",
			"Null" => "NO",
			"Key" => "",
			"Default" => NULL,
			"Extra" => ""
		]));
	}

}