<?php

namespace Test\Generators;

use jasonstanley\clementine\Generators\DefaultEnum;
use jasonstanley\clementine\Schema\Integer;
use \PHPUnit\Framework\TestCase;

class DefaultEnumTest extends TestCase
{

	public function testEnumGeneratesAValidValue()
	{
		$column = $this->makeEnumColumn();

		$generator = new DefaultEnum();

		// We loop 100 times here to ensure all enum values are covered.
		$values = [];
		for ($i = 0; $i < 100; $i++) {
			$values[$generator->getValue($column)] = true;
		}
		foreach ($values as $value => $_) {
			$this->assertTrue(in_array($value, ['1', '2', '3']));
		}

		// Check random values are generated.
		$this->assertGreaterThan(1, count($values));
	}

	public function testEnumAppliesOnlyToEnumColumns()
	{
		$enumColumn = $this->makeEnumColumn();
		$intColumn = $this->makeIntColumn();
		$generator = new DefaultEnum();
		$this->assertTrue($generator->applies($enumColumn));
		$this->assertFalse($generator->applies($intColumn));
	}

	private function makeEnumColumn()
	{
		return (new \jasonstanley\clementine\Schema\Enum([
			"Field" => "enum",
			"Type" => "enum('1', '2', '3')",
			"Null" => "NO",
			"Key" => "",
			"Default" => NULL,
			"Extra" => ""
		]));
	}

	private function makeIntColumn()
	{
		return (new Integer([
			"Field" => "int",
			"Type" => "int(11)",
			"Null" => "NO",
			"Key" => "",
			"Default" => NULL,
			"Extra" => ""
		]));
	}

}