<?php

namespace Test\Generators;

use jasonstanley\clementine\Generators\ColumnBetween;
use jasonstanley\clementine\Schema\DateTime;
use jasonstanley\clementine\Schema\Integer;
use \PHPUnit\Framework\TestCase;

class ColumnBetweenTest extends TestCase
{

	public function testColumnBetweenAppliesToTheCorrectColumn()
	{
		$generator = new ColumnBetween('int', 1, 3);

		$this->assertTrue($generator->applies($this->makeIntColumn()));
		$this->assertFalse($generator->applies($this->makeDateTimeColumn()));
	}

	public function testColumnBetweenGeneratesAnIntegerBetweenTwoGivenIntegers()
	{
		$generator = new ColumnBetween('int', 1, 20);
		$column = $this->makeIntColumn();
		for ($i = 0; $i < 50; $i++) {
			$this->assertInternalType('int', $i);
			$this->assertThat(
				$generator->getValue($column),
				$this->logicalAnd(
					$this->greaterThan(0),
					$this->lessThan(21)
				)
			);
		}
	}

	public function testColumnBetweenGeneratesADateTimeBetweenTwoGivenDateTime()
	{
		$start = new \DateTime('-1 day');
		$startTimeString = $start->format('Y-m-d H:i:s');
		$end = new \DateTime();
		$endTimeString = $end->format('Y-m-d H:i:s');

		$generator = new ColumnBetween('datetime', $start, $end);
		$column = $this->makeDateTimeColumn();
		for ($i = 0; $i < 50; $i++) {
			$value = $generator->getValue($column);
			$this->assertInstanceOf('\DateTime', \DateTime::createFromFormat('Y-m-d H:i:s', $value));
			$this->assertThat(
				$value,
				$this->logicalAnd(
					$this->greaterThan($startTimeString),
					$this->lessThan($endTimeString)
				)
			);
		}
	}

	public function testDateTimeSuppliedForIntColumnProducesAnInt()
	{
		$start = new \DateTime('-1 day');
		$startTimestamp = $start->getTimestamp();
		$end = new \DateTime();
		$endTimestamp = $end->getTimestamp();

		$generator = new ColumnBetween('datetime', $start, $end);
		$column = $this->makeIntColumn();

		$this->assertInternalType('int', $generator->getValue($column));
		$this->assertThat(
			$generator->getValue($column),
			$this->logicalAnd(
				$this->greaterThan($startTimestamp),
				$this->lessThan($endTimestamp)
			)
		);

	}

	private function makeIntColumn()
	{
		return (new Integer([
			"Field" => "int",
			"Type" => "int(11)",
			"Null" => "NO",
			"Key" => "",
			"Default" => NULL,
			"Extra" => ""
		]));
	}

	private function makeDateTimeColumn()
	{
		return (new DateTime([
			"Field" => "datetime",
			"Type" => "datetime",
			"Null" => "NO",
			"Key" => "",
			"Default" => NULL,
			"Extra" => ""
		]));
	}

}