<?php

namespace Test\Generators;

use jasonstanley\clementine\Generators\ColumnEqualTo;
use jasonstanley\clementine\Schema\DateTime;
use jasonstanley\clementine\Schema\Integer;
use PHPUnit\Framework\TestCase;

class ColumnEqualToTest extends TestCase
{

	public function testColumnEqualToAppliesToTheCorrectColumn()
	{
		$generator = new ColumnEqualTo('int', 1);

		$this->assertTrue($generator->applies($this->makeIntColumn()));
		$this->assertFalse($generator->applies($this->makeDateTimeColumn()));
	}

	public function testColumnEqualTo()
	{
		$generator = new ColumnEqualTo('int', 1);
		$this->assertEquals(1, $generator->getValue($this->makeIntColumn()));
	}

	private function makeIntColumn()
	{
		return (new Integer([
			"Field" => "int",
			"Type" => "int(11)",
			"Null" => "NO",
			"Key" => "",
			"Default" => NULL,
			"Extra" => ""
		]));
	}

	private function makeDateTimeColumn()
	{
		return (new DateTime([
			"Field" => "datetime",
			"Type" => "datetime",
			"Null" => "NO",
			"Key" => "",
			"Default" => NULL,
			"Extra" => ""
		]));
	}
}