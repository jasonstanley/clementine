<?php
namespace Test;
use jasonstanley\clementine\Clementine;
use jasonstanley\clementine\Stores\PDOStore;
use \PHPUnit\Framework\TestCase;

class ClementineTest extends TestCase {

	private $connection;

	public function setUp() {
		if ( ! $this->connection) {
			$this->connection = new \PDO('mysql:host=localhost;dbname=test;', 'homestead', 'secret');
			$this->connection->query("
				DROP TABLE IF EXISTS `types`;
				CREATE TABLE `types` (
				  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				  `varchar` varchar(255) NOT NULL DEFAULT '',
				  `tinyint` tinyint(1) NOT NULL,
				  `smallint` smallint(4) NOT NULL,
				  `mediumint` mediumint(6) NOT NULL,
				  `int` int(10) NOT NULL,
				  `bigint` bigint(14) NOT NULL,
				  `tinytext` tinytext NOT NULL,
				  `text` text NOT NULL,
				  `mediumtext` mediumtext NOT NULL,
				  `longtext` longtext NOT NULL,
				  `enum` enum('a','b','c') NOT NULL DEFAULT 'a',
				  `datetime` datetime NOT NULL,
				  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
			");
		}
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testExceptionIsThrownWithoutTable() {
		$clem = new Clementine(new PDOStore($this->connection));
		$clem->make(1);
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testExceptionIsThrownWithoutRows() {
		$clem = new Clementine(new PDOStore($this->connection));
		$clem->table('users')->make(0);
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testExceptionIsThrownWithNegativeRows() {
		$clem = new Clementine(new PDOStore($this->connection));
		$clem->table('users')->make(-1);
	}

	public function testADummyRowIsGenerated() {
		$clem = new Clementine(new PDOStore($this->connection));
		$clem->table('types')->make(3);

		$this->assertEquals(3, $this->totalRows('types'));
	}

	public function testARowCanBeGivenASetValue() {
		$clem = new Clementine(new PDOStore($this->connection));
		$clem->with('int', $clem->equalTo(12))->table('types')->make(10);

		$stmt = $this->connection->query("SELECT COUNT(*) AS total FROM `types` WHERE `int` = 12");
		$this->assertEquals(10, $stmt->fetch()['total']);

		// Check everything isn't set to 10.
		$stmt = $this->connection->query("SELECT COUNT(*) AS total FROM `types` WHERE `smallint` = 12");
		$this->assertNotEquals(10, $stmt->fetch()['total']);
	}

	public function testARowCanBeGivenARangeOfValues() {
		$clem = new Clementine(new PDOStore($this->connection));
		$clem->with('int', $clem->between(new \DateTime('-1 day'), new \DateTime()))->table('types')->make(10);

		$stmt = $this->connection->query("SELECT COUNT(*) AS total FROM `types` WHERE `int` < UNIX_TIMESTAMP()");
		$this->assertEquals(10, $stmt->fetch()['total']);

		// @todo Remove these tests into the relevant generator test.
		$clem->with('int', $clem->between(new \DateTime(), new \DateTime('+1 day')))->table('types')->make(10);
		$stmt = $this->connection->query("SELECT COUNT(*) AS total FROM `types` WHERE `int` > UNIX_TIMESTAMP()");
		$this->assertEquals(10, $stmt->fetch()['total']);

		$clem->with('int', $clem->between(4, 10))->table('types')->make(10);
		$stmt = $this->connection->query("SELECT COUNT(*) AS total FROM `types` WHERE `int` BETWEEN 4 AND 10");
		$this->assertEquals(10, $stmt->fetch()['total']);
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testInvalidTableThrowsException() {
		$clem = new Clementine(new PDOStore($this->connection));
		$clem->table('doesNotExist')->make(1);
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testConfigurationIsResetBetweenRuns() {
		$this->connection->query("TRUNCATE `types`");

		$clem = new Clementine(new PDOStore($this->connection));
		$clem->with('int', $clem->equalTo('10'))
			->table('types')
			->make(5);

		$clem->make(2);
	}

	protected function totalRows(string $table) {
		$stmt = $this->connection->query("SELECT COUNT(*) AS total FROM {$table}");
		return $stmt->fetch()['total'];
	}



}